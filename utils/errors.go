package utils

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"reflect"

	"github.com/go-chi/jwtauth"
	"github.com/go-chi/render"
)

// IsValid must be implemented by models structs to validate if the model/json is correct (missing fields)
type IsValid interface {
	IsValid() error
}

type ErrJSONUnmarshal error

func NewErrJSONUnmarshal(from string, to reflect.Type) ErrJSONUnmarshal {
	return errors.New(fmt.Sprintf("json: cannot unmarshal %s into type %v", from, to))
}

// Library errors
var (
	ErrUnauthorized  = errors.New("jwtauth: token is unauthorized")
	ErrExpired       = errors.New("jwtauth: token is expired")
	ErrNBFInvalid    = errors.New("jwtauth: token nbf validation failed")
	ErrIATInvalid    = errors.New("jwtauth: token iat validation failed")
	ErrNoTokenFound  = errors.New("jwtauth: no token found")
	ErrAlgoInvalid   = errors.New("jwtauth: algorithm mismatch")
	ErrNoUsernameErr = errors.New("jwtauth: no username")
	ErrNoUsername    = &ErrResponse{
		Err:            ErrNoUsernameErr,
		ErrorText:      ErrNoUsernameErr.Error(),
		HTTPStatusCode: http.StatusUnauthorized,
		HTTPStatusText: http.StatusText(http.StatusUnauthorized),
		Code:           99,
		Field:          "Header/Authorization",
		Ressource:      "",
	}

	ErrNoUUIDErr = errors.New("jwtauth: no uuid")
	ErrNoUUID    = &ErrResponse{
		Err:            ErrNoUUIDErr,
		ErrorText:      ErrNoUUIDErr.Error(),
		HTTPStatusCode: http.StatusUnauthorized,
		HTTPStatusText: http.StatusText(http.StatusUnauthorized),
		Code:           99,
		Field:          "Header/Authorization",
		Ressource:      "",
	}

	ErrNoQueryType  = errors.New("map: no query type")
	ErrNoMapExists  = errors.New("map: no image exists")
	ErrMissingField = errors.New("missing field")
)

type ErrResponse struct {
	Err       error  `json:"-"`               // low-level runtime error
	ErrorText string `json:"error,omitempty"` // low-level runtime error string

	HTTPStatusCode int    `json:"status_code"` // http response status code
	HTTPStatusText string `json:"status"`      // http status message

	Code      int    `json:"Code"`      // The code associated with this error (application-specific error code).
	Field     string `json:"field"`     // The specific field or aspect of the resource associated with this error.
	Ressource string `json:"ressource"` // The type of resource associated with this error.
}

func (e ErrResponse) Error() string {
	if e.Err != nil {
		return e.Err.Error()
	}
	return ""
}

// Render renders the error with "Content-Type: application/json" and the apprpiated HTTP Status Code
func (e *ErrResponse) Render(w http.ResponseWriter, r *http.Request) error {
	w.Header().Add("Content-Type", "application/json")
	render.Status(r, e.HTTPStatusCode)
	return nil

}

// NewErrResponseGeneric returns a generic ErrResponse render.Renderer
func NewErrResponseGeneric(err error, httpStatusCode int, httpStatusText string, code int, field string, ressource string) render.Renderer {
	return &ErrResponse{
		Err:            err,
		ErrorText:      err.Error(),
		HTTPStatusCode: httpStatusCode,
		HTTPStatusText: httpStatusText,
		Code:           code,
		Field:          field,
		Ressource:      ressource,
	}
}

// NewErrResponseSpecifc return a specific http error render.Renderer
func NewErrResponseSpecifc(err error, httpStatusCode int, code int, field string, ressource string) render.Renderer {
	switch httpStatusCode {
	case http.StatusUnprocessableEntity:
		// 422
		// The server understands the content type of the request entity (hence a 415 Unsupported Media Type status code is inappropriate), and the syntax of the request entity is correct (thus a 400 Bad Request status code is inappropriate) but was unable to process the contained instructions.
		return NewErrResponseGeneric(err, http.StatusUnprocessableEntity, http.StatusText(http.StatusUnprocessableEntity), code, field, ressource)
	case http.StatusBadRequest:
		// 400
		// The server cannot or will not process the request due to something that is perceived to be a client error (e.g., malformed request syntax, invalid request message framing, or deceptive request routing).
		return NewErrResponseGeneric(err, http.StatusBadRequest, http.StatusText(http.StatusBadRequest), code, field, ressource)
	case http.StatusNotFound:
		// 404
		// The origin server did not find a current representation for the target resource or is not willing to disclose that one exists.
		return NewErrResponseGeneric(err, http.StatusNotFound, http.StatusText(http.StatusNotFound), code, field, ressource)
	case http.StatusInternalServerError:
		return NewErrResponseGeneric(err, http.StatusInternalServerError, http.StatusText(http.StatusInternalServerError), 100, field, ressource)
	default:
		return NewErrResponseGeneric(err, http.StatusBadRequest, http.StatusText(http.StatusBadRequest), code, field, ressource)
	}

}

// DecodeUUIDErrRes get thes jwt uuid from the jwt token
func DecodeUUIDErrRes(r *http.Request, ressource string) (string, error) {
	var errRes ErrResponse
	var uuid string
	_, claims, err := jwtauth.FromContext(r.Context())
	if err != nil {
		errRes.Err = err
		errRes.ErrorText = ErrNoUUIDErr.Error()
		errRes.Field = "uuid"
		errRes.HTTPStatusCode = http.StatusBadRequest
		errRes.HTTPStatusText = http.StatusText(http.StatusBadRequest)
		errRes.Code = 1
		errRes.Ressource = ressource
		return uuid, errRes
	}
	uuid, ok := claims["uuid"].(string)
	if !ok {
		errRes.Err = errors.New("uuid assertion not possible")
		errRes.ErrorText = ErrNoUUIDErr.Error()
		errRes.Field = "uuid"
		errRes.HTTPStatusCode = http.StatusBadRequest
		errRes.HTTPStatusText = http.StatusText(http.StatusBadRequest)
		errRes.Code = 1
		errRes.Ressource = ressource
		return uuid, errRes
	}
	return uuid, nil
}

/*
type UnmarshalTypeError struct {
        Value  string       // description of JSON value - "bool", "array", "number -5"
        Type   reflect.Type // type of Go value it could not be assigned to
        Offset int64        // error occurred after reading Offset bytes; added in Go 1.5
        Struct string       // name of the struct type containing the field; added in Go 1.8
        Field  string       // name of the field holding the Go value; added in Go 1.8
}
*/

// DecodeErrRes unmarshals the r.Body json to the given struct and returns the structs.IsValid() error or the error for specific field (for example trying to convert "string" to "int")
func DecodeErrRes(r *http.Request, v IsValid, ressource string) error {
	var errRes ErrResponse
	if err := json.NewDecoder(r.Body).Decode(v); err != nil {
		fmt.Println(err)
		if terr, ok := err.(*json.UnmarshalTypeError); ok {
			errRes.ErrorText = NewErrJSONUnmarshal(terr.Value, terr.Type).Error()
			errRes.Field = terr.Field

		} else {
			errRes.ErrorText = err.Error()
		}
		errRes.Err = err
		errRes.HTTPStatusCode = http.StatusBadRequest
		errRes.HTTPStatusText = http.StatusText(http.StatusBadRequest)
		errRes.Code = 1
		errRes.Ressource = ressource
		return errRes
	}
	return v.IsValid()
}
