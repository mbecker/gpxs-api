package utils

import (
	"github.com/rs/zerolog/log"
)

// FailOnError logs fatal the error
func FailOnError(err error, msg string) {
	if err != nil {
		log.Fatal().Err(err).Msg(msg)
	}
}

func Message(status bool, message string) map[string]interface{} {
	return map[string]interface{}{"status": status, "message": message}
}
func MessageSuccess(message string, gpxID int64, xidMap string) map[string]interface{} {
	return map[string]interface{}{"status": true, "message": message, "gpxid": gpxID, "xid": xidMap}
}
func MessageError(message string) map[string]interface{} {
	return map[string]interface{}{"status": false, "error": message}
}
