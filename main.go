package main

import (
	"net/http"
	"path/filepath"
	"strings"
        "fmt"
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/cors"
	"github.com/go-chi/jwtauth"
	"github.com/go-chi/render"
	_ "github.com/lib/pq"
	"github.com/rs/xid"
	"github.com/rs/zerolog/log"
	"gitlab.com/mbecker/gpxs-api/features/gpx"
	"gitlab.com/mbecker/gpxs-api/features/maps"
	"gitlab.com/mbecker/gpxs-api/internal/config"
	"gitlab.com/mbecker/gpxs-api/utils"
)

var applicationName = "gpxs-api"
var applicationID string
var tokenAuth *jwtauth.JWTAuth

type Me struct {
	Uuid     string `json:"uuid"`
	Username string `json:"username"`
}

func failOnError(err error, msg string) {
	if err != nil {
		log.Fatal().Err(err).Msg(msg)
	}
}

func failOnErrors(key string, errs []error, msg string) {
	if len(errs) > 0 {
		log.Fatal().Errs(key, errs).Msg(msg)
	}
}

func init() {
	applicationID = xid.New().String()
	log.Logger = log.With().Timestamp().Str("applicationName", applicationName).Str("applicationID", applicationID).Logger() // Caller()
	// zerolog.TimeFieldFormat = "" -- Use for timestamp
	log.Info().Msg("Initialized application started")

	// JWT token
	tokenAuth = jwtauth.New("HS256", []byte("jwt-gpxs-strava-secret"), nil)

	// For debugging/example purposes, we generate and print
	// a sample jwt token with claims `user_id:123` here:
	// _, tokenString, _ := tokenAuth.Encode(jwt.MapClaims{"user_id": 123})
	// fmt.Printf("JWT example is %s\n\n", tokenString)
}

func Routes(configuration *config.Config) *chi.Mux {

	// Router - chi
	router := chi.NewRouter()

	// Basic CORS
	// for more ideas, see: https://developer.github.com/v3/#cross-origin-resource-sharing
	cors := cors.New(cors.Options{
		// AllowedOrigins: []string{"https://foo.com"}, // Use this to allow specific origin hosts
		AllowedOrigins: []string{"*"},
		// AllowOriginFunc:  func(r *http.Request, origin string) bool { return true },
		AllowedMethods: []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowedHeaders: []string{"Access-Control-Request-Headers", "Accept", "Authorization", "Content-Type", "X-CSRF-Token"},
		// RequestHeaders:   []string{"Access-Control-Request-Headers", "Accept", "Authorization", "Content-Type", "X-CSRF-Token"},
		ExposedHeaders:   []string{"Link"},
		AllowCredentials: true,
		MaxAge:           300, // Maximum value not ignored by any of major browsers
	})

	router.Group(func(r chi.Router) {
		r.Use(
			render.SetContentType(render.ContentTypeJSON), // Set content-Type headers as application/json
			middleware.Logger,                             // Log API request calls
			middleware.DefaultCompress,                    // Compress results, mostly gzipping assets and json
			middleware.RedirectSlashes,                    // Redirect slashes to no slash URL versions
			middleware.Recoverer,                          // Recover from panics without crashing server
			cors.Handler,
		)
		filesDir := filepath.Join(configuration.Constants.Maps.Directory)
                fmt.Println("static files dir:", filesDir)
		FileServer(r, configuration.Constants.ServerDirectoryStatic, http.Dir(filesDir))
	})

	router.Group(func(r chi.Router) {
		r.Use(
			render.SetContentType(render.ContentTypeJSON), // Set content-Type headers as application/json
			middleware.Logger,                             // Log API request calls
			middleware.DefaultCompress,                    // Compress results, mostly gzipping assets and json
			middleware.RedirectSlashes,                    // Redirect slashes to no slash URL versions
			middleware.Recoverer,                          // Recover from panics without crashing server
			cors.Handler,
			jwtauth.Verifier(tokenAuth),
			jwtauth.Authenticator,
		)
		r.Route("/v1", func(r chi.Router) {
			r.Mount("/api/gpx", gpx.New(configuration).Routes())
			r.Mount("/api/maps", maps.New(configuration).Routes())
			r.Get("/me", func(w http.ResponseWriter, r *http.Request) {
				_, claims, _ := jwtauth.FromContext(r.Context())
				username, ok := claims["username"].(string)
				if !ok {
					render.Render(w, r, utils.ErrNoUsername)
					return
				}
				uuid, ok := claims["uuid"].(string)
				if !ok {
					render.Render(w, r, utils.ErrNoUUID)
					return
				}
				me := Me{
					Uuid:     username,
					Username: uuid,
				}
				render.JSON(w, r, me)
			})
		})
	})

	return router
}

// FileServer conveniently sets up a http.FileServer handler to serve
// static files from a http.FileSystem.
func FileServer(r chi.Router, path string, root http.FileSystem) {
	if strings.ContainsAny(path, "{}*") {
		panic("FileServer does not permit URL parameters.")
	}

	fs := http.StripPrefix(path, http.FileServer(root))

	if path != "/" && path[len(path)-1] != '/' {
		r.Get(path, http.RedirectHandler(path+"/", 301).ServeHTTP)
		path += "/"
	}
	path += "*"

	r.Get(path, http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fs.ServeHTTP(w, r)
	}))
}

func main() {
	configuration, err := config.New()
	if err != nil {
		failOnError(err, "Configuration error")
	}

	// Router
	router := Routes(configuration)

	// walkFunc := func(method string, route string, handler http.Handler, middlewares ...func(http.Handler) http.Handler) error {
	// 	log.Printf("%s %s\n", method, route) // Walk and print out all routes
	// 	return nil
	// }
	// if err := chi.Walk(router, walkFunc); err != nil {
	// 	failOnError(err, "chi walk router")
	// }

	if err = http.ListenAndServe(":"+configuration.Constants.PORT, router); err != nil {
		log.Error().Err(err).Msg("http ListenAndServe error")
	} else {
		log.Info().Str("http", "start").Msg("HTTP server listening on port: " + configuration.Constants.PORT)
	}
}
