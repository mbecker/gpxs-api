package config

import (
	"github.com/gomodule/redigo/redis"
)

// NewRedisPool returns a pool of redis connection with maxIdle and maxActive connections
func NewRedisPool(addr string, password string, db int) *redis.Pool {
	return &redis.Pool{
		// Maximum number of idle connections in the pool.
		MaxIdle: 80,
		// max number of connections
		MaxActive: 12000,
		// Dial is an application supplied function for creating and
		// configuring a connection.
		Dial: func() (redis.Conn, error) {
			c, err := redis.Dial("tcp", addr)
			if err != nil {
				panic(err.Error())
			}
			return c, err
		},
	}
}

// ping tests connectivity for redis (PONG should be returned)
func ping(c redis.Conn) (string, error) {
	// Send PING command to Redis
	pong, err := c.Do("PING")
	if err != nil {
		return "", err
	}

	// PING command returns a Redis "Simple String"
	// Use redis.String to convert the interface type to string
	s, err := redis.String(pong, err)
	if err != nil {
		return "", err
	}

	// Output: PONG

	return s, nil
}
