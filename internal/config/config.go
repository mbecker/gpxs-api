package config

import (
	"database/sql"
	"fmt"
	"log"

	"github.com/fsnotify/fsnotify"
	"github.com/gomodule/redigo/redis"
	_ "github.com/lib/pq"
	"github.com/spf13/viper"
	gpxsamqplib "gitlab.com/mbecker/gpxs-amqp-lib"
	"gitlab.com/mbecker/gpxs-api/utils"
)

// Constants defines the Config Keys
type Constants struct {
	PORT                  string
	ServerURL             string
	ServerDirectoryStatic string
	Maps                  struct {
		Directory string
	}
	DB struct {
		Connection string
	}
	AMQP struct {
		URL   string
		QUEUE string
	}
	Redis struct {
		URL      string
		Password string
		Database int
	}
}

// func (c Constants) String() string {
// 	return fmt.Sprintf("{ServerURL:%s, Title:%, Name:%s}", c.ServerURL, p.Title, p.Name)
// }

// Config has the env paramaters for the app and the db connection
type Config struct {
	Constants
	Database  *sql.DB
	Queue     *gpxsamqplib.Queue
	RedisPool *redis.Pool
}

// New is used to generate a configuration instance which will be passed around the codebase
func New() (*Config, error) {
	config := Config{}
	constants, err := initViper()
	config.Constants = constants
	if err != nil {
		return &config, err
	}

	/**
	 * TODO:
	 * - [x] Add pglib initialization
	 */
	/**
	 * TODO:
	 * - [] Read and update how connection could be new established if it's gone
	 */
	config.Database, err = sql.Open("postgres", config.Constants.DB.Connection)
	config.Database.SetMaxOpenConns(10)
	config.Database.SetMaxIdleConns(10)
	utils.FailOnError(err, "DB Connection can't be established")
	// defer config.Database.Close()
	err = config.Database.Ping()
	utils.FailOnError(err, "DB Connection Ping error")

	// Initialize AMQP Connection
	config.Queue = gpxsamqplib.New(config.Constants.AMQP.QUEUE, config.Constants.AMQP.URL)
	defer config.Queue.Close()

	// Redis Initialization
	config.RedisPool = NewRedisPool(config.Constants.Redis.URL, config.Constants.Redis.Password, config.Constants.Redis.Database)
	conn := config.RedisPool.Get()
	defer conn.Close()

	pong, err := ping(conn)
	if err != nil {
		utils.FailOnError(err, "Redis connection can't be established")
	}
	fmt.Println("--- REDIS ---")
	fmt.Printf("PING Response = %s\n", pong)
	fmt.Println("--- REDIS ---")

	return &config, err
}

func initViper() (Constants, error) {
	viper.SetConfigName("api.config") // Configuration fileName without the .TOML or .YAML extension
	viper.AddConfigPath("./config")   // Search the root directory for the configuration file
	err := viper.ReadInConfig()       // Find and read the config file
	if err != nil {                   // Handle errors reading the config file
		return Constants{}, err
	}
	viper.WatchConfig() // Watch for changes to the configuration file and recompile
	viper.OnConfigChange(func(e fsnotify.Event) {
		fmt.Println("Config file changed:", e.Name)
	})
	viper.SetDefault("PORT", "8080")
	viper.SetDefault("ServerDirectoryStatic", "/static")
	if err = viper.ReadInConfig(); err != nil {
		log.Panicf("Error reading config file, %s", err)
	}

	var constants Constants
	err = viper.Unmarshal(&constants)
	fmt.Println("--- CONFIG START ---")
	fmt.Printf("%+v\n", constants)
	fmt.Println("--- CONFIG END ---")
	return constants, err
}
