docker stop gpxs-api && docker rm gpxs-api && \
docker run --detach \
    --name gpxs-api \
    --network gpxs-net \
    --env "VIRTUAL_HOST=api.mobility.digital" \
    --env "LETSENCRYPT_HOST=api.mobility.digital" \
    --env "LETSENCRYPT_EMAIL=mats.becker@gmail.com" \
    --publish 3000:3000 \
    --expose 3000 \
    --volume /home/mbecker/docker/volumes/maps:/maps \
    --volume /home/mbecker/apps/gpxs-api/config:/config \
    gpxs-api   
