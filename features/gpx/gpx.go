package gpx

import (
	"database/sql"
	"errors"
	"fmt"
	"net/http"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/go-chi/chi"
	"github.com/go-chi/render"
	"github.com/golang/protobuf/proto"
	"github.com/golang/protobuf/ptypes/timestamp"
	"github.com/rs/xid"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	polyline "github.com/twpayne/go-polyline"
	"gitlab.com/mbecker/gpxs-api/internal/config"
	"gitlab.com/mbecker/gpxs-api/utils"
	"gitlab.com/mbecker/gpxs-proto/apiv9"
)

type gpxStrava struct {
	Polyline        *string             `json:"polyline,omitempty"`
	Points          []*apiv9.Point      `json:"-"`
	Distance        *float64            `json:"distance"`
	MovingTime      *int64              `json:"moving_time,omitempty"`
	StravaID        *int64              `json:"strava_id,omitempty"`
	ActivityName    *string             `json:"activity_name,omitempty"`
	ActivityTpe     *int32              `json:"activity_type,omitempty"`
	ActivityColor   string              `json:"activity_color"` // Optional
	ImagePresetType *int32              `json:"image_preset_type,omitempty"`
	TileProvider    *int32              `json:"tile_provider,omitempty"`
	Privacy         []int32             `json:"privacy"` // Optional
	PrivacyType     []apiv9.PrivacyType `json:"-"`       // Optional

}

func (gpxs *gpxStrava) IsValid() error {
	var errRes utils.ErrResponse
	errRes.HTTPStatusCode = http.StatusBadRequest
	errRes.HTTPStatusText = http.StatusText(http.StatusBadRequest)
	errRes.Code = 1
	errRes.Ressource = "gpx/map"
	errRes.Err = utils.ErrMissingField
	errRes.ErrorText = utils.ErrMissingField.Error()
	if gpxs.Polyline == nil || len(*gpxs.Polyline) == 0 {
		errRes.Field = "polyline"
		return errRes
	} else if gpxs.Distance == nil {
		errRes.Field = "distance"
		return errRes
	} else if gpxs.MovingTime == nil {
		errRes.Field = "moving_time"
		return errRes
	} else if gpxs.StravaID == nil {
		errRes.Field = "strava_id"
		return errRes
	} else if gpxs.ActivityName == nil || len(*gpxs.ActivityName) == 0 {
		errRes.Field = "activity_name"
		return errRes
	} else if gpxs.ActivityTpe == nil {
		errRes.Field = "activity_type"
		return errRes
	} else if _, ok := apiv9.ActivityType_name[*gpxs.ActivityTpe]; !ok {
		errRes.Field = "activity_type"
		return errRes
	} else if gpxs.ImagePresetType == nil {
		errRes.Field = "image_preset_type"
		return errRes
	} else if _, ok = apiv9.ImagePresetType_name[*gpxs.ImagePresetType]; !ok {
		errRes.Field = "image_preset_type"
		return errRes
	} else if gpxs.TileProvider == nil {
		errRes.Field = "tile_provider"
		return errRes
	} else if _, ok = apiv9.TileProviderType_name[*gpxs.TileProvider]; !ok {
		errRes.Field = "tile_provider"
		return errRes
	}

	gpxs.PrivacyType = make([]apiv9.PrivacyType, len(gpxs.Privacy))
	for i, p := range gpxs.Privacy {
		gpxs.PrivacyType[i] = apiv9.PrivacyType(p)
	}

	if match, _ := regexp.MatchString(`(?i)(^#[0-9A-F]{6}$)|(^#[0-9A-F]{3}$)`, gpxs.ActivityColor); !match {
		gpxs.ActivityColor = "default"
	}

	return nil
}

func createLogDict(activity *apiv9.Activity) *zerolog.Event {
	dict := zerolog.Dict()
	dict.Int64("ActivityID", activity.ActivityID)
	dict.Str("MapID", activity.MapID)
	dict.Str("Uuid", activity.Uuid)
	dict.Int32("ActivityType", int32(activity.ActivityType))
	dict.Str("ActivityName", activity.ActivityName)
	dict.Int32("ImagePresetType", int32(activity.ImagePresetType))
	dict.Int32("TileProvider", int32(activity.TileProviderType))
	dict.Str("ActivityColor", activity.ActivityColor)
	return dict
}

type Config struct {
	*config.Config
}

func New(configuration *config.Config) *Config {
	return &Config{configuration}
}

func (config *Config) Routes() *chi.Mux {
	router := chi.NewRouter()
	router.Post("/strava", config.CreateGpxFromStrava)
	// router.Delete("/{todoID}", config.DeleteTodo)
	// router.Post("/", config.CreateTodo)
	router.Get("/strava/activities/{stravaID}", config.getLocalActivityByID)
	router.Post("/strava/activities", config.getStravaActivities)
	return router
}

type StravaActivity struct {
	StravaID       int64  `json:"-"`
	StravaIDString string `json:"activityID"`
}
type MapPrivacy struct {
	Private   bool `json:private`
	Community bool `json:private`
	Public    bool `json:private`
}
type MapImage struct {
	Url    string `json:"url"`
	Width  int    `json:"width"`
	Height int    `json:"height"`
}
type StravaMap struct {
	Preset    string     `json:preset`
	Type      string     `json:type`
	Privacy   MapPrivacy `json:privacy`
	Image     MapImage   `json:"image"`
	Resized   MapImage   `json:"image"`
	Thumb     MapImage   `json:"image"`
	CreatedAt string     `json:"created_at"`
}

func (config *Config) getStravaActivities(w http.ResponseWriter, r *http.Request) {

}

func (config *Config) getLocalActivityByID(w http.ResponseWriter, r *http.Request) {
	ressource := "gpx/strava/{activityID}"
	id := strings.TrimSpace(chi.URLParam(r, "stravaID"))

	stravaID, err := strconv.ParseInt(id, 0, 64)
	if err != nil {
		render.Render(w, r, utils.NewErrResponseSpecifc(err, http.StatusUnprocessableEntity, 2, "stravaID", ressource))
		return
	}
	if stravaID < 1 {
		render.Render(w, r, utils.NewErrResponseSpecifc(errors.New("stravaID must be an positive integer"), http.StatusUnprocessableEntity, 2, "stravaID", ressource))
		return
	}

	/*
			 select
			shard_1.maps.preset,
			shard_1.maps."type",
			shard_1.maps.community,
			shard_1.maps.public,
			shard_1.maps.private,
			shard_1.maps.filename,
			shard_1.maps.width,
			shard_1.maps.height,
			shard_1.maps.resized_filename,
			shard_1.maps.resized_width,
			shard_1.maps.resized_height,
			shard_1.maps.thumb_filename,
			shard_1.maps.resized_width,
			shard_1.maps.resized_height,
			shard_1.maps.created_at
		from
			shard_1.maps
		inner join shard_1.activities on
			shard_1.activities.id = shard_1.maps.activity_id
		where
			shard_1.activities.strava_id = 2172487848;
	*/

	var rows *sql.Rows
	rows, err = config.Database.Query("select			shard_1.maps.preset,			shard_1.maps.type,			shard_1.maps.community,			shard_1.maps.public,			shard_1.maps.private,			shard_1.maps.filename,			shard_1.maps.width,			shard_1.maps.height,			shard_1.maps.resized_filename,			shard_1.maps.resized_width,			shard_1.maps.resized_height,			shard_1.maps.thumb_filename,			shard_1.maps.thumb_width,			shard_1.maps.thumb_height, shard_1.maps.created_at from shard_1.maps inner join shard_1.activities on shard_1.activities.id = shard_1.maps.activity_id where shard_1.activities.strava_id = $1", stravaID)
	defer rows.Close()

	if err != nil {
		render.Render(w, r, utils.NewErrResponseSpecifc(err, http.StatusUnprocessableEntity, 2, "stravaID", ressource))
		return
	}

	baseURL := fmt.Sprintf("%s%s", config.Constants.ServerURL, config.Constants.ServerDirectoryStatic)
	mapsData := []StravaMap{}
	for rows.Next() {
		mapsOne := StravaMap{}
		privacyOne := MapPrivacy{}
		mapsOne.Image = MapImage{}
		mapsOne.Resized = MapImage{}
		mapsOne.Thumb = MapImage{}
		err = rows.Scan(
			&mapsOne.Preset,
			&mapsOne.Type,
			&privacyOne.Community,
			&privacyOne.Public,
			&privacyOne.Private,
			&mapsOne.Image.Url,
			&mapsOne.Image.Width,
			&mapsOne.Image.Height,
			&mapsOne.Resized.Url,
			&mapsOne.Resized.Width,
			&mapsOne.Resized.Height,
			&mapsOne.Thumb.Url,
			&mapsOne.Thumb.Width,
			&mapsOne.Thumb.Height,
			&mapsOne.CreatedAt,
		)
		if err != nil {
			log.Error().Err(err)
		}

		mapsOne.Image.Url = fmt.Sprintf("%s/%s", baseURL, mapsOne.Image.Url)
		mapsOne.Resized.Url = fmt.Sprintf("%s/%s", baseURL, mapsOne.Resized.Url)
		mapsOne.Thumb.Url = fmt.Sprintf("%s/%s", baseURL, mapsOne.Thumb.Url)

		mapsOne.Privacy = privacyOne

		mapsData = append(mapsData, mapsOne)

	}

	render.JSON(w, r, mapsData)
	return
}

func (config *Config) CreateGpxFromStrava(w http.ResponseWriter, r *http.Request) {
	log.Info().Str("route", "gpx").Str("path", "strava").Str("action", "post").Msg("gpx strava post decoded polyline")
	ressource := "gpx/strava"
	uuid, err := utils.DecodeUUIDErrRes(r, ressource)
	// uuid, err := utils.GetUUID(r)
	if err != nil {
		if terr, ok := err.(utils.ErrResponse); ok {
			render.Render(w, r, &terr)
			return
		}
		render.Render(w, r, utils.ErrNoUUID)
		return
	}
	log.Info().Str("route", "gpx").Str("path", "strava").Str("action", "post").Str("uuid", uuid).Msg("gpx strava post uuid")

	var g gpxStrava
	if err := utils.DecodeErrRes(r, &g, ressource); err != nil {
		log.Error().Err(err).Str("path", "strava").Str("action", "post").Str("uuid", uuid).Str("err", err.Error()).Msg("error in request")
		if terr, ok := err.(utils.ErrResponse); ok {
			render.Render(w, r, &terr)
			return
		}
		render.Render(w, r, utils.NewErrResponseSpecifc(err, http.StatusInternalServerError, 100, "", ""))
		return
	}

	buf := []byte(*g.Polyline)
	coords, _, err := polyline.DecodeCoords(buf)
	if err != nil {
		log.Info().Str("route", "gpx").Str("path", "strava").Str("action", "post").Str("uuid", uuid).Str("err", err.Error()).Msg("error in decoding polyline")
		render.Render(w, r, utils.NewErrResponseSpecifc(err, http.StatusUnprocessableEntity, 2, "polyline", ressource))
		return
	}

	/**
	 * Activty message: Type Strava Activity
	 */
	// Get the activity id from the db
	var activityID int64
	err = config.Database.QueryRow(`INSERT INTO shard_1.activities(uuid, name, strava_id) VALUES ($1, $2, $3) returning (shard_1.activities.id);`, uuid, &g.ActivityName, &g.StravaID).Scan(&activityID)
	if err != nil {
		log.Error().Err(err).Str("route", "map").Str("path", "map").Str("action", "get").Str("uuid", uuid).Msg("Erorr DB insert to get activity id")
		render.Render(w, r, utils.NewErrResponseSpecifc(errors.New("SQL Error"), http.StatusUnprocessableEntity, 3, "", ressource))
		return
	}
	if activityID == 0 {
		err = errors.New("Activity ID = 0")
		log.Error().Err(err).Str("route", "map").Str("path", "map").Str("action", "get").Str("uuid", uuid).Msg("Erorr DB insert to get activity id")
		render.Render(w, r, utils.NewErrResponseSpecifc(errors.New("SQL Error"), http.StatusUnprocessableEntity, 3, "", ressource))
		return
	}

	mapID := xid.New().String()

	g.Points = make([]*apiv9.Point, len(coords))
	for i, point := range coords {
		g.Points[i] = &apiv9.Point{
			Lat:  point[0],
			Long: point[1],
		}
	}

	apiActvityMessage := apiv9.Activity{
		GpxType:          apiv9.GpxType_Strava,
		Uuid:             uuid,
		ActivityID:       activityID,
		ActivityIDString: strconv.FormatInt(activityID, 10),
		MapID:            mapID,
		ActivityName:     *g.ActivityName,
		ActivityType:     apiv9.ActivityType(*g.ActivityTpe),
		ActivityColor:    g.ActivityColor,
		ImagePresetType:  apiv9.ImagePresetType(*g.ImagePresetType),
		TileProviderType: apiv9.TileProviderType(*g.TileProvider),
		PrivacyType:      g.PrivacyType,
		CreatedAt: &timestamp.Timestamp{
			Seconds: time.Now().Unix(),
		},
		GpxsActivity: nil,
		StravaActivity: &apiv9.StravaActivity{
			StravaID:   *g.StravaID,
			Points:     g.Points,
			Distance:   *g.Distance,
			MovingTime: *g.MovingTime,
		},
	}

	err = sendMessage(config, apiActvityMessage)
	if err != nil {
		fmt.Println("--- AMQP MESSAGE SENT ERROR ---")
		log.Error().Err(err).Msg("AMQP Message couldn't be published")
	} else {
		fmt.Println("--- AMQP MESSAGE SENT SUCCESS ---")
		log.Info().Dict("gpxMessage", createLogDict(&apiActvityMessage)).Msg("AMQP Message sent")
	}

	render.JSON(w, r, apiActvityMessage) // Return some demo response
	return
}

func sendMessage(config *Config, apiActvityMessage apiv9.Activity) error {
	var data []byte
	data, err := proto.Marshal(&apiActvityMessage)
	if err != nil {
		return err
	}
	return config.Queue.Push(data)
}
