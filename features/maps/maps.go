package maps

import (
	"database/sql"
	"fmt"
	"image"
	_ "image/jpeg"
	_ "image/png"
	"net/http"
	"os"
	"path/filepath"
	"strconv"

	"github.com/go-chi/chi"
	"github.com/go-chi/jwtauth"
	"github.com/go-chi/render"
	"github.com/rs/zerolog/log"
	"gitlab.com/mbecker/gpxs-api/internal/config"
	"gitlab.com/mbecker/gpxs-api/utils"
)

type Image struct {
	Url    string `json:"url"`
	Width  int    `json:"width"`
	Height int    `json:"height"`
}

type Map struct {
	Image   Image  `json:"image,omitempty"`
	Resized Image  `json:"resized,omitempty"`
	Thumb   Image  `json:"thumb,omitempty"`
	Error   string `json:"error,omitempty"`
}

type MapType struct {
	Type   string  `json:"type"`
	URL    string  `json:"url"`
	Width  float64 `json:"width"`
	Height float64 `json:"height"`
}

type Maps struct {
	ActivitiyID int64              `json:"activity_id"`
	ID          string             `json:"id"`
	Preset      string             `json:"preset"`
	Name        string             `json:"name"`
	CreatedAt   string             `json:"created_at"`
	Maps        map[string]MapType `json:"maps,omitempty"`
}

type Config struct {
	*config.Config
}

func New(configuration *config.Config) *Config {
	return &Config{configuration}
}

func (config *Config) Routes() *chi.Mux {
	router := chi.NewRouter()
	router.Get("/", config.GetMaps)
	router.Get("/{mapID}", config.GetMap)
	// router.Delete("/{todoID}", config.DeleteTodo)
	// router.Post("/", config.CreateTodo)
	// router.Get("/", config.GetAllTodos)
	return router
}

func chekFileExists(file string) error {
	_, err := os.Stat(file)
	if err == nil {
		// fmt.Printf("%s exists\n", file)
		return nil
	} else if os.IsNotExist(err) {
		// fmt.Printf("%s exists not (!)\n", file)
		return fmt.Errorf("file %s not exists", file)
	} else {
		// fmt.Printf("%s stats error: %v\n", file, err)
		return fmt.Errorf("file %s stat error: %v", file, err)
	}
}

func getImageDimension(imagePath string) (int, int) {
	file, err := os.Open(imagePath)
	if err != nil {
		fmt.Fprintf(os.Stderr, "%v\n", err)
	}

	image, _, err := image.DecodeConfig(file)
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s: %v\n", imagePath, err)
	}

	return image.Width, image.Height
}

// GetMaps get all map images type(thumb,resized,original) for the jwt(uuid)
func (config *Config) GetMaps(w http.ResponseWriter, r *http.Request) {
	log.Info().Str("route", "map").Str("path", "map").Str("action", "get").Msg("map get")
	_, claims, _ := jwtauth.FromContext(r.Context())
	uuid, ok := claims["uuid"].(string)
	if !ok {
		render.Render(w, r, utils.ErrNoUUID)
		return
	}
	log.Info().Str("route", "map").Str("path", "map").Str("uuid", uuid).Msg("uuid")

	var err error
	var page int
	offsetQueryParam := r.URL.Query().Get("offset")
	page, err = strconv.Atoi(offsetQueryParam)
	if err != nil {
		page = 0
	}

	var rows *sql.Rows
	rows, err = config.Database.Query(`
						select 
							activities.id, 
							maps.id, 
							maps.preset, 
							activities.name,
							maps.thumb_filename, 
							maps.thumb_width, 
							maps.thumb_height,
							maps.resized_filename, 
							maps.resized_width, 
							maps.resized_height,
							maps.filename, 
							maps.width, 
							maps.height, 
							maps.created_at 
						from 
							shard_1.maps inner join shard_1.activities 
						on 
							maps.activity_id = activities.id
							where 
							activities.uuid = $1 
						order by 
							maps.created_at desc
						limit
							10
						offset
							$2`,
		uuid, page)

	// SELECT name, resized_filename, resized_width, resized_height FROM strava_maps WHERE uuid = $1

	if err != nil {
		log.Info().Str("route", "map").Str("path", "map").Str("action", "get").Str("uuid", uuid).Str("err", err.Error()).Msg("error in request")
		render.Render(w, r, utils.NewErrResponseSpecifc(err, http.StatusBadRequest, 1, "query/type", "maps"))
		return
	}

	// Close sql rows
	// Defer is used to ensure that a function call is performed later in a program's execution, usually for purposes of cleanup
	defer rows.Close()

	baseURL := fmt.Sprintf("%s%s", config.Constants.ServerURL, config.Constants.ServerDirectoryStatic)
	mapsData := []Maps{}

	for rows.Next() {
		mapsOne := Maps{}
		thumb := MapType{}
		resized := MapType{}
		original := MapType{}

		err = rows.Scan(
			&mapsOne.ActivitiyID,
			&mapsOne.ID,
			&mapsOne.Preset,
			&mapsOne.Name,

			&thumb.URL,
			&thumb.Width,
			&thumb.Height,

			&resized.URL,
			&resized.Width,
			&resized.Height,

			&original.URL,
			&original.Width,
			&original.Height,

			&mapsOne.CreatedAt,
		)
		thumb.Type = "thumb"
		thumb.URL = fmt.Sprintf("%s/%s", baseURL, thumb.URL)
		resized.Type = "resized"
		resized.URL = fmt.Sprintf("%s/%s", baseURL, resized.URL)
		original.Type = "original"
		original.URL = fmt.Sprintf("%s/%s", baseURL, original.URL)

		maps := make(map[string]MapType)
		maps["thumb"] = thumb
		maps["resized"] = resized
		maps["original"] = original
		mapsOne.Maps = maps

		if err != nil {
			log.Error().Err(err)
		}

		mapsData = append(mapsData, mapsOne)

	}

	log.Info().Str("route", "map").Str("path", "map").Str("action", "get").Str("uuid", uuid).Str("err", "nil").Msg("send response")
	render.JSON(w, r, mapsData) // A chi router helper for serializing and returning json
}

// GetMap checks for a given mapID if the image on the disk exists
func (config *Config) GetMap(w http.ResponseWriter, r *http.Request) {
	mapID := chi.URLParam(r, "mapID")

	baseURL := fmt.Sprintf("%s%s", config.Constants.ServerURL, config.Constants.ServerDirectoryStatic)

	maps := Map{}
	mapImage := Image{}
	resizedImage := Image{}
	thumbImage := Image{}

	imageName := fmt.Sprintf("%s.png", mapID)
	imageNameJpeg := fmt.Sprintf("%s.jpeg", mapID)
	resizedName := fmt.Sprintf("%s_resized.jpeg", mapID)
	thumbName := fmt.Sprintf("%s_thumb.jpeg", mapID)

	imagePath := filepath.Join(config.Constants.Maps.Directory, imageName)
	imagePathJpeg := filepath.Join(config.Constants.Maps.Directory, imageNameJpeg)
	resizedPath := filepath.Join(config.Constants.Maps.Directory, resizedName)
	thumbPath := filepath.Join(config.Constants.Maps.Directory, thumbName)

	if err := chekFileExists(imagePath); err == nil {
		mapImage.Url = fmt.Sprintf("%s/%s", baseURL, imageName)
		mapImage.Width, mapImage.Height = getImageDimension(imagePath)
		maps.Image = mapImage
	} else {
		// the original image could be .jpeg or *.png
		if err := chekFileExists(imagePathJpeg); err == nil {
			mapImage.Url = fmt.Sprintf("%s/%s", baseURL, imageNameJpeg)
			mapImage.Width, mapImage.Height = getImageDimension(imagePathJpeg)
			maps.Image = mapImage
		}
	}

	if err := chekFileExists(resizedPath); err == nil {
		resizedImage.Url = fmt.Sprintf("%s/%s", baseURL, resizedName)
		resizedImage.Width, resizedImage.Height = getImageDimension(resizedPath)
		maps.Resized = resizedImage
	}

	if err := chekFileExists(thumbPath); err == nil {
		thumbImage.Url = fmt.Sprintf("%s/%s", baseURL, thumbName)
		thumbImage.Width, thumbImage.Height = getImageDimension(thumbPath)
		maps.Thumb = thumbImage
	}

	if len(maps.Image.Url) == 0 && len(maps.Resized.Url) == 0 && len(maps.Thumb.Url) == 0 {
		render.Render(w, r, utils.NewErrResponseSpecifc(utils.ErrNoMapExists, http.StatusUnprocessableEntity, 99, "", "maps"))
	} else {
		render.JSON(w, r, maps) // A chi router helper for serializing and returning json
	}
	return
}

// func (config *Config) DeleteTodo(w http.ResponseWriter, r *http.Request) {
// 	response := make(map[string]string)
// 	response["message"] = "Deleted TODO successfully"
// 	render.JSON(w, r, response) // Return some demo response
// }

// func (config *Config) CreateTodo(w http.ResponseWriter, r *http.Request) {
// 	response := make(map[string]string)
// 	response["message"] = "Created TODO successfully"
// 	render.JSON(w, r, response) // Return some demo response
// }

// func (config *Config) GetAllTodos(w http.ResponseWriter, r *http.Request) {
// 	todos := []Todo{
// 		{
// 			Slug:  "slug",
// 			Title: "Hello world",
// 			Body:  "Heloo world from planet earth",
// 		},
// 	}
// 	render.JSON(w, r, todos) // A chi router helper for serializing and returning json
// }
