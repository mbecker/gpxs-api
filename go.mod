module gitlab.com/mbecker/gpxs-api

require (
	github.com/BurntSushi/toml v0.3.1 // indirect
	github.com/fsnotify/fsnotify v1.4.7
	github.com/go-chi/chi v4.0.1+incompatible
	github.com/go-chi/cors v1.0.0
	github.com/go-chi/jwtauth v0.0.0-20190109153619-47840abb19b3
	github.com/go-chi/render v1.0.1
	github.com/go-redis/redis v6.15.1+incompatible
	github.com/golang/protobuf v1.2.0
	github.com/gomodule/redigo v2.0.0+incompatible
	github.com/lib/pq v1.0.0
	github.com/onsi/gomega v1.4.3 // indirect
	github.com/rs/xid v1.2.1
	github.com/rs/zerolog v1.11.0
	github.com/spf13/afero v1.2.1 // indirect
	github.com/spf13/viper v1.3.1
	github.com/streadway/amqp v0.0.0-20190214183023-884228600bc9 // indirect
	github.com/twpayne/go-polyline v0.0.0-20170530103353-ed808842d5ea
	gitlab.com/mbecker/gpxs-amqp-lib v0.0.0-20190220111439-21ac52c0a103
	gitlab.com/mbecker/gpxs-proto v0.0.0-20190221151518-70aeea92ae9c
	golang.org/x/net v0.0.0-20190213061140-3a22650c66bd // indirect
	golang.org/x/sync v0.0.0-20181221193216-37e7f081c4d4 // indirect
	golang.org/x/sys v0.0.0-20190221075227-b4e8571b14e0 // indirect
)
